package main

import (
	"fmt"
	"log"
	"os"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/joho/godotenv"
)

var db *gorm.DB
var err error

type Task struct {
	gorm.Model
	Description string
	Prompt      string
	Commands    string // Kubectl commands to execute
}

func dbConnectionString() string {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	host := os.Getenv("DB_HOST")
	port := os.Getenv("DB_PORT")
	user := os.Getenv("DB_USER")
	password := os.Getenv("DB_PASSWORD")
	dbname := os.Getenv("DB_NAME")

	return fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
}

func main() {
	// NOTE: replace with your own database connection
	db, err = gorm.Open("postgres", dbConnectionString())

	if err != nil {
		fmt.Println("db err: ", err)
	}
	db.AutoMigrate(&Task{})

	r := gin.Default()

	// Add CORS middleware
	config := cors.DefaultConfig()
	config.AllowAllOrigins = true
	r.Use(cors.New(config))

	r.GET("/tasks/:id", GetTask)
	r.GET("/tasks/", GetTasks)
	r.POST("/tasks/", CreateTask)
	r.PUT("/tasks/:id", UpdateTask)
	r.DELETE("/tasks/:id", DeleteTask)

	r.Run(":8085")
}

func GetTask(c *gin.Context) {
	id := c.Params.ByName("id")
	var task Task
	if err := db.Where("id = ?", id).First(&task).Error; err != nil {
		c.AbortWithStatus(404)
		fmt.Println(err)
	} else {
		c.JSON(200, task)
	}
}

func GetTasks(c *gin.Context) {
	var tasks []Task
	if err := db.Find(&tasks).Error; err != nil {
		c.AbortWithStatus(404)
		fmt.Println(err)
	} else {
		c.JSON(200, tasks)
	}
}

func CreateTask(c *gin.Context) {
	var task Task
	c.BindJSON(&task)

	if err := db.Create(&task).Error; err != nil {
		c.AbortWithStatus(404)
		fmt.Println(err)
	} else {
		c.JSON(200, task)
	}
}

func UpdateTask(c *gin.Context) {
	id := c.Params.ByName("id")
	var task Task
	if err := db.First(&task, id).Error; err != nil {
		c.AbortWithStatus(404)
		fmt.Println(err)
	} else {
		c.BindJSON(&task)
		db.Save(&task)
		c.JSON(200, task)
	}
}

func DeleteTask(c *gin.Context) {
	id := c.Params.ByName("id")
	var task Task
	d := db.Where("id = ?", id).Delete(&task)
	fmt.Println(d)
	c.JSON(200, gin.H{"id #" + id: "deleted"})
}
