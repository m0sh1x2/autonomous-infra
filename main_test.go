package main_test

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	main "gitlab.com/m0sh1x2/autonomous-infra"
)

var router *gin.Engine
var db *gorm.DB

func setup() {
	db, _ = gorm.Open("sqlite3", "/tmp/gorm_test.db")
	db.AutoMigrate(&main.Task{})
	router = gin.Default()
	router.GET("/tasks/:id", main.GetTask)
	router.GET("/tasks/", main.GetTasks)
	router.POST("/tasks/", main.CreateTask)
	router.PUT("/tasks/:id", main.UpdateTask)
	router.DELETE("/tasks/:id", main.DeleteTask)
}

func teardown() {
	db.DropTableIfExists(&main.Task{})
	db.Close()
}

func TestIntegration(t *testing.T) {
	setup()
	defer teardown()

	t.Run("CRUD operations", func(t *testing.T) {
		t.Run("CreateTask", testCreateTask)
		t.Run("GetTask", testGetTask)
		t.Run("GetTasks", testGetTasks)
		t.Run("UpdateTask", testUpdateTask)
		t.Run("DeleteTask", testDeleteTask)
	})
}

func testCreateTask(t *testing.T) {
	task := main.Task{
		Description: "Sample Task",
		Prompt:      "Sample Prompt",
		Commands:    "Sample Commands",
	}
	jsonTask, _ := json.Marshal(task)
	req, _ := http.NewRequest("POST", "/tasks/", bytes.NewBuffer(jsonTask))
	req.Header.Set("Content-Type", "application/json")
	resp := httptest.NewRecorder()

	router.ServeHTTP(resp, req)

	if resp.Code != http.StatusOK {
		t.Errorf("Expected status %v; got %v", http.StatusOK, resp.Code)
	}

	// Assert the response body if necessary
	// ...

	// Optionally, you can retrieve the created task from the database and assert its values
	// ...
}

func testGetTask(t *testing.T) {
	// Create a task in the database using the GORM API before testing the retrieval
	task := main.Task{
		Description: "Sample Task",
		Prompt:      "Sample Prompt",
		Commands:    "Sample Commands",
	}
	db.Create(&task)

	req, _ := http.NewRequest("GET", "/tasks/1", nil)
	resp := httptest.NewRecorder()

	router.ServeHTTP(resp, req)

	if resp.Code != http.StatusOK {
		t.Errorf("Expected status %v; got %v", http.StatusOK, resp.Code)
	}

	// Assert the response body if necessary
	// ...
}

func testGetTasks(t *testing.T) {
	// Create multiple tasks in the database using the GORM API before testing the retrieval
	task1 := main.Task{
		Description: "Sample Task 1",
		Prompt:      "Sample Prompt 1",
		Commands:    "Sample Commands 1",
	}
	task2 := main.Task{
		Description: "Sample Task 2",
		Prompt:      "Sample Prompt 2",
		Commands:    "Sample Commands 2",
	}
	db.Create(&task1)
	db.Create(&task2)

	req, _ := http.NewRequest("GET", "/tasks/", nil)
	resp := httptest.NewRecorder()

	router.ServeHTTP(resp, req)

	if resp.Code != http.StatusOK {
		t.Errorf("Expected status %v; got %v", http.StatusOK, resp.Code)
	}

	// Assert the response body if necessary
	// ...
}

func testUpdateTask(t *testing.T) {
	// Create a task in the database using the GORM API before testing the update
	task := main.Task{
		Description: "Sample Task",
		Prompt:      "Sample Prompt",
		Commands:    "Sample Commands",
	}
	db.Create(&task)

	task.Description = "Updated Task"
	jsonTask, _ := json.Marshal(task)
	req, _ := http.NewRequest("PUT", "/tasks/1", bytes.NewBuffer(jsonTask))
	req.Header.Set("Content-Type", "application/json")
	resp := httptest.NewRecorder()

	router.ServeHTTP(resp, req)

	if resp.Code != http.StatusOK {
		t.Errorf("Expected status %v; got %v", http.StatusOK, resp.Code)
	}

	// Assert the response body if necessary
	// ...
}

func testDeleteTask(t *testing.T) {
	// Create a task in the database using the GORM API before testing the deletion
	task := main.Task{
		Description: "Sample Task",
		Prompt:      "Sample Prompt",
		Commands:    "Sample Commands",
	}
	db.Create(&task)

	req, _ := http.NewRequest("DELETE", "/tasks/1", nil)
	resp := httptest.NewRecorder()

	router.ServeHTTP(resp, req)

	if resp.Code != http.StatusOK {
		t.Errorf("Expected status %v; got %v", http.StatusOK, resp.Code)
	}

	// Assert the response body if necessary
	// ...
}
